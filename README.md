# docker-alpine-ansible

A Docker image of Alpine Linux with Ansible for use in GitLab CI/CD pipelines.

Build
```
REGISTRY=registry.gitlab.com/konradp/docker-alpine-ansible
docker build -t $REGISTRY/alpine:ansible .
docker push $REGISTRY/alpine:ansible
```
