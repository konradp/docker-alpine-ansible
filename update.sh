#!/bin/bash
REGISTRY=registry.gitlab.com/konradp/docker-alpine-ansible
docker login registry.gitlab.com
docker build -t $REGISTRY/alpine:ansible .
docker push $REGISTRY/alpine:ansible
